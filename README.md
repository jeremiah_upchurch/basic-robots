# BASIC-RoBots

## What is BASIC-RoBots ?

BASIC-RoBots is a real-time strategy game about programming robots to colonize a planet.

## How to launch this game ?

BASIC-RoBots depends on Python2, PyGame and PyOpenGL.

For Debian/Ubuntu Linux:

    $ sudo apt-get install python2.6 python-pygame python-opengl
    $ cd /path/to/basic-robots/
    $ make

## How to control robots ?

The robots can only be controlled with command line. Press [Ctrl+N], or [ENTER] key when you select a robot, to open a terminal on the selected robot.

There are 2 types of commands:

  - **Internal commands** : Internal controls of the operating system (Ex : `cd`, `mkdir`, `let`, `edit`, ...)
  - **External commands** : commands to interact with the robot and the outside world (Ex : `forward`, `rotleft`, `build`, ...)

Type `intcmd` to show the internal commands of the robots and `extcmd` to show external commands.

### Examples
#### Move forward and rotate the robot

    > forward
    forward-> 1
    > rotleft
    rotleft-> 1

#### Build a Tipper with a Mothership

    > help build
    ...
    > robots
    Id  Name
    0   Woodcutter
    1   Tipper
    2   Thermal Power Station
    robots-> 0
    > let A 1
    > build

#### Cut a tree with a Woodcutter

    > cuttree
    > inv
    Id  Item      Qty
    1   Sapling   2
    0   Wood      8

    Capacity: 100000 g
    Weight: 80004 g
    Free: 19996 g (19 %)

## Robots features
### File system

Each robot has its own file system. So, feel free to create folders and files in the robots with the commands `mkdir`, `touch`, `edit`, ...

### Registers
#### Read-write

A robot owns 4 registers of 1 byte each that can be written with the command `let` (usable in BASIC-script and in terminal) :

  - `A`
  - `B`
  - `C`
  - `D`

#### Read only

The read-only registers can only be used in expressions. It's forbidden to assign value to this registers.

  - `O` : Overflow flag. Gets the value 1 if an arithmetic overflow has occurred in the last expression, 0 otherwise.
  - `SH` : Shield value of the robot. Not use in this version
  - `EN` : Energy value of the robot.
  - `X` : first component (in Cartesian coordinates) of the position of the robot
  - `Y` : second component (in Cartesian coordinates) of the position of the robot
  - `K` : contains keyboard inputs

### Stack

A robot also owns a LIFO memory, useful for storing data temporarily. You can see the content of the stack with the command `stack`. See the BASIC commands `PUSH` and `POP` for details on usage.

## BASIC Programming Language

The robots can be programmed with a specific language based on [BASIC][1]. You can edit a program with the command `edit FILE`, run it with `run FILE` and debug it with `debug FILE`.

### Syntax
#### Input and Output
  - `PRINT` : displays a message on the terminal
  - `READ` : asks the user to enter the value of a register
  - `LET` : assigns a value (which may be the result of an expression) to a register
  - `CALL` : call an external command and assigns the return to a register
  - `PUSH` : push the value of a register in the stack
  - `POP` : pop the value from the top of the stack to a register

#### Program flow control
  - `IF ... THEN ... ELSE` : used to perfoms comparaisons or make decisions
  - `WHILE ... ENDWHILE` : repeat a section of code while the specified condition is true
  - `LABEL` : labels current line
  - `GOTO` : jumps to a labelled line in the program
  - `GOTOSUB` : temporarily jumps to a labelled line, returning to the following line after encountering the RETURN command

#### Miscellaneous
  - `REM` : holds a programmer's comment
  - `INCLUDE` : includes the content of an other file to the current program

### Examples

#### Loop example

    REM *** Moves the robot 5 squares forward ***
    REM Store the value of the register A
    PUSH A

    REM Set the value of A to 4
    LET A 4

    REM Repeat while the value of A is positive
    WHILE A > 0

       REM Decrement the value of A
       LET A A-1

       REM Call the external command "forward"
       CALL forward

    ENDWHILE

    REM Get the stored value of A
    POP A

#### Use the standard library

    REM Move the robot to the entered coordinates
    REM Include the file lib/move
    INCLUDE lib/move

    PUSH A
    PUSH B

    PRINT Current position :
    PRINT ( $X , $Y )
    PRINT A : X - B : Y

    REM Ask the user to enter coordinates
    READ A
    READ B

    REM Call subroutine defined in lib/move
    GOTOSUB move_to

    POP B
    POP A

[1]: http://en.wikipedia.org/wiki/BASIC