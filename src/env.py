##
## env.py for BASIC-RoBots
## 
## Copyright (C) 2012 Pierre Surply
## <pierre.surply@gmail.com>
##
## This file is part of BASIC-RoBots.
##
##    BASIC-RoBots is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    BASIC-RoBots is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with BASIC-RoBots.  If not, see <http://www.gnu.org/licenses/>. 
##
## Started on  Thu Jun 28 13:22:05 2012 Pierre Surply
## Last update Wed Sep  5 13:50:20 2012 Pierre Surply
##

import os
import random
import math
import array
import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *

import surface
import noise
import savable
from robot import mothership, woodcutter

time_day = 64000

class Env(savable.Savable):
    temp_tiles = [((255, 255, 255, 255), 0, "Grass", 0.5),\
                  ((150, 100, 0, 255), 1, "Dirt", 0.4),\
                  ((0, 0, 100, 255), 2, "Water", 0),\
                  ((255, 255, 150, 255), 3, "Sand", 0.2),\
                  ((0, 0, 150, 255), 4, "Deep water", 0),\
                  ((100, 100, 100, 255), 5, "Stone", 0.6),\
                  ((255, 0, 0, 255), 6, "Lava", 0),\
                  ((20, 20, 20, 255), 7, "Gravel", 0.4),\
                  ((10, 10, 10, 255), 8, "Volvanic stone", 0.6),\
                  ((100, 0, 100, 255), 9, "Meteorite dirt", 0.6),\
                  ((100, 0, 255, 255), 10, "Meteorite stone", 0.6),\
                  ((101, 101, 101, 255), 11, "Stone", 0.6)]

    temp_elts = [((0, 100, 0, 255), 0, "Tree", 0),\
                     ((200, 200, 200, 255), 1, "Snow tree", 0),\
                     ((0, 50, 0, 255), 2, "Dead tree", 0),\
                     ((0, 150, 0, 255), 3, "Infected tree", 0),\
                     ((0, 90, 0, 255), 4, "Cut tree", 0),\
                     ((150, 150, 150, 255), 5, "Stone", 0),\
                     ((253, 253, 253, 255), 6, "Snow", 2),\
                     ((255, 255, 0, 255), 7, "Light", 0)]

    biomes = [([(0.3, 4),\
                   (0.8,2),\
                   (1.0,3)], []),\
                  ([(0.5, 2),\
                       (0.7, 3),\
                       (0.8, 1),\
                       (1.0, 0)], []),\
                  ([(0.3, 2),\
                       (0.33, 3),\
                       (0.6, 1),\
                       (1.0, 0)], \
                       [(0.2, 5), (0.25, 2), (0.4, 0)]),\
                  ([(0.1, 2),\
                       (0.2, 1),\
                       (0.4, 11),\
                       (0.6, 5),\
                       (0.7, 7),\
                       (0.8, 8),\
                       (1.0, 6)], \
                       [(0.3, 0), \
                            (0.4, 2),\
                            (0.5, 5)]),\
                  ([(0.5, 6),\
                       (0.8, 8),\
                       (1.0, 5)], [(0.3, 5),\
                                       (0.5, 2)]),\
                  ([(0.2, 2),\
                       (0.5, 1),\
                       (0.7, 11),\
                       (1.0, 5)], \
                       [(0.5, 1), \
                            (0.9, 6)]),\
                  ([(0.2, 2),\
                       (0.21,3),\
                       (0.4,1),\
                       (0.8,0),\
                       (1.0,5)], [(0.5, 0)]),\
                  ([(0.5, 6),\
                       (0.8, 8),\
                       (1.0, 5)], [(0.4, 3)]),\
                  ([(0.1, 2),\
                       (0.3, 9),\
                       (0.6, 10),\
                       (0.8, 5),\
                       (1.0, 11)], [(0.4, 3)]),\
                  ([(0.4, 4),\
                       (0.8, 2),\
                       (1.0, 3)], []),\
                  ([(0.1, 2),\
                       (0.2, 1),\
                       (0.4, 11),\
                       (0.6, 5),\
                       (0.7, 7),\
                       (0.8, 8),\
                       (1.0, 6)], [])]
    size = 64
    
    def __init__(self, path, biome, (x, y), events):
        self.set_display_list()
        self.init_tile()
        self.angle = 0
        self.robots = []
        self.running_robot = 0
        self.biome = biome
        self.x = x
        self.y = y
        self.events = events
        self.path = path
        self.path_img = "saves/" + path + "/env/" + str(x) + "_" + str(y) + ".bmp"
        self.path_img_elts = "saves/" + path + "/env/" + str(x) + "_" + str(y) + "_elts.bmp"
        if os.path.isfile(self.path_img):
            self.load()
        else:
            self.build(biome)
            self.save()
        self.sprite_floor = surface.cut_surface(pygame.image.load("res/img/floor.png").convert_alpha(), (16, 16))
        self.sprite_elts = surface.cut_surface(pygame.image.load("res/img/lvl_elts.png").convert_alpha(), (16, 16))
        self.sprite_robots = surface.cut_surface(pygame.image.load("res/img/robots.png").convert_alpha(), (64, 32))
        self.tps_anim_sprite = 0
        self.id_sprite_water = 0
        self.sprite_water = surface.cut_surface(pygame.image.load("res/img/water.png").convert_alpha(), (16, 16))
        self.cursor = surface.surface2texture(pygame.image.load("res/img/cursor.png").convert_alpha())


    def set_display_list(self):
        self.list_floor = glGenLists(2)
        glNewList(self.list_floor,GL_COMPILE)
        glBegin(GL_QUADS)
        glTexCoord2d(0,0); glVertex3d(0, 1, 0)
        glTexCoord2d(0,1); glVertex3d(0, 0, 0)
        glTexCoord2d(1,1); glVertex3d(1, 0, 0)
        glTexCoord2d(1,0); glVertex3d(1, 1, 0)
        glEnd()
        glEndList()

        self.list_floor_side = glGenLists(3)
        glNewList(self.list_floor_side,GL_COMPILE)
        glBegin(GL_QUADS)
        glTexCoord2d(0,0); glVertex3d(0, 0, 1)
        glTexCoord2d(0,1); glVertex3d(0, 0, 0)
        glTexCoord2d(1,1); glVertex3d(1, 0, 0)
        glTexCoord2d(1,0); glVertex3d(1, 0, 1)
        glEnd()
        glEndList()

        self.list_elt = glGenLists(3)
        glNewList(self.list_elt,GL_COMPILE)
        #glBegin(GL_QUADS)
        #glTexCoord2d(0,0); glVertex3d(-0.5, -0.5, 0)
        #glTexCoord2d(0,1); glVertex3d(-0.5, -0.5, 1)
        #glTexCoord2d(1,1); glVertex3d(0.5, 0.5, 1)
        #glTexCoord2d(1,0); glVertex3d(0.5, 0.5, 0)
        #glEnd()
        #glBegin(GL_QUADS)
        #glTexCoord2d(0,0); glVertex3d(-0.5, 0.5, 0)
        #glTexCoord2d(0,1); glVertex3d(-0.5, 0.5, 1)
        #glTexCoord2d(1,1); glVertex3d(0.5, -0.5, 1)
        #glTexCoord2d(1,0); glVertex3d(0.5, -0.5, 0)
        #glEnd()
        glBegin(GL_QUADS)
        glTexCoord2d(0,0); glVertex3d(0, 0.5, 0)
        glTexCoord2d(0,1); glVertex3d(0, 0.5, 1)
        glTexCoord2d(1,1); glVertex3d(0, -0.5, 1)
        glTexCoord2d(1,0); glVertex3d(0, -0.5, 0)
        glEnd()
        glEndList()

        self.list_elt_cube = glGenLists(3)
        glNewList(self.list_elt_cube,GL_COMPILE)
        glBegin(GL_QUADS)
        glTexCoord2d(0,0); glVertex3d(0, 1, 1)
        glTexCoord2d(0,1); glVertex3d(0, 0, 1)
        glTexCoord2d(1,1); glVertex3d(1, 0, 1)
        glTexCoord2d(1,0); glVertex3d(1, 1, 1)
        glEnd()
        glBegin(GL_QUADS)
        glTexCoord2d(0,0); glVertex3d(0, 1, 1)
        glTexCoord2d(0,1); glVertex3d(0, 1, 0)
        glTexCoord2d(1,1); glVertex3d(1, 1, 0)
        glTexCoord2d(1,0); glVertex3d(1, 1, 1)
        glEnd()
        glBegin(GL_QUADS)
        glTexCoord2d(0,0); glVertex3d(0, 0, 1)
        glTexCoord2d(0,1); glVertex3d(0, 0, 0)
        glTexCoord2d(1,1); glVertex3d(1, 0, 0)
        glTexCoord2d(1,0); glVertex3d(1, 0, 1)
        glEnd()
        glBegin(GL_QUADS)
        glTexCoord2d(0,0); glVertex3d(0, 0, 1)
        glTexCoord2d(0,1); glVertex3d(0, 0, 0)
        glTexCoord2d(1,1); glVertex3d(0, 1, 0)
        glTexCoord2d(1,0); glVertex3d(0, 1, 1)
        glEnd()
        glBegin(GL_QUADS)
        glTexCoord2d(0,0); glVertex3d(1, 0, 1)
        glTexCoord2d(0,1); glVertex3d(1, 0, 0)
        glTexCoord2d(1,1); glVertex3d(1, 1, 0)
        glTexCoord2d(1,0); glVertex3d(1, 1, 1)
        glEnd()
        glEndList()

        self.list_select = glGenLists(3)
        glNewList(self.list_select,GL_COMPILE)
        glBegin(GL_QUADS)
        glTexCoord2d(0,0); glVertex3d(-0.5, 0.5, 0)
        glTexCoord2d(0,1); glVertex3d(-0.5, -0.5, 0)
        glTexCoord2d(1,1); glVertex3d(0.5, -0.5, 0)
        glTexCoord2d(1,0); glVertex3d(0.5, 0.5, 0)
        glEnd()
        glEndList()

        self.quad = gluNewQuadric()
        gluQuadricDrawStyle(self.quad, GLU_FILL)
        gluQuadricTexture(self.quad, GL_TRUE);
        self.list_robot = glGenLists(3)
        glNewList(self.list_robot,GL_COMPILE)
        gluSphere(self.quad, 0.3, 16, 16)
        glEndList()
        
    def init_tile(self):
        self.tile = array.array('B', [0] * (self.size**2))
        self.tile_elts = array.array('B', [0xFF] * (self.size**2))
        self.light = array.array('B', [0] * (self.size**2))

    def load(self):
        self.load_img(self.path_img, self.size, self.tile, self.color2tile)
        if os.path.isfile(self.path_img_elts):
            self.load_img(self.path_img_elts,\
                              self.size,\
                              self.tile_elts,\
                              self.color2elt)
        else:
            self.build_elts()
            self.save_img(self.path_img_elts, self.size, self.tile_elts, self.elt2color)
            
    def save(self):
        self.save_img(self.path_img, self.size, self.tile, self.tile2color)
        self.save_img(self.path_img_elts, self.size, self.tile_elts, self.elt2color)
        for i in self.robots:
            i.save()

    def tile2color(self, (x, y)):
        tile = self.tile[(y*self.size)+x]
        for i in self.temp_tiles:
            if tile == i[1]:
                return pygame.Color(i[0][0],\
                                        i[0][1],\
                                        i[0][2],\
                                        i[0][3])
        
    def color2tile(self, color):
        for i in self.temp_tiles:
            if color == i[0]:
                return i[1]
        return 0

    def elt2color(self, (x, y)):
        elt = self.tile_elts[(y*self.size)+x]
        for i in self.temp_elts:
            if elt == i[1]:
                return pygame.Color(i[0][0],\
                                        i[0][1],\
                                        i[0][2],\
                                        i[0][3])
        return pygame.Color(255, 255, 255)

    def color2elt(self, color):
        if (color != (255, 255, 255)):
            for i in self.temp_elts:
                if color == i[0]:
                    return i[1]
            return 0xFF
        else:
            return 0xFF

    def get_daylight(self):
        t = pygame.time.get_ticks() % time_day
        if t < time_day / 4 or t > 3 * time_day / 4:
            return 0
        elif time_day / 4 <= t <= time_day / 2:
            return 0xFF * (4 * (t-time_day/4))/time_day
        else:
            return 0xFF - (0xFF * (4 * (t-time_day/2))/time_day)

    def calculate_light(self):
        self.light = array.array('B', [0] * (self.size**2))
        for i in self.robots:
            self.rec_calculate_light((i.pos_x, i.pos_y), 0, 0xB0)
        for y in range(self.size):
            for x in range(self.size):
                if self.tile[(y*self.size)+x] == 6:
                    self.rec_calculate_light((x, y), 0, 0x80)

    def rec_calculate_light(self, (org_x, org_y), dist, intens):
        if intens > 0:
            for x in range(max(org_x, 0), min(org_x+dist+1, self.size)):
                self.set_light((x, org_y), intens)
                if org_y+dist < self.size:
                    self.set_light((x, org_y+dist), intens)
            for y in range(max(org_y+1, 0), min(org_y+dist, self.size)):
                self.set_light((org_x, y), intens)
                if org_x+dist < self.size:
                    self.set_light((org_x+dist, y), intens)
            self.rec_calculate_light((org_x-1, org_y-1), dist+2, intens-0x44)

    def set_light(self, (x, y), intens):
        org = self.light[(y*self.size)+x]
        light = org + intens
        if light > 0xFF:
            light = 0xFF
        self.light[(y*self.size)+x] = light

    def set_color(self, (x, y)):
        l = 0
        if x >= 0 and x < self.size and\
                y >=0 and y < self.size:
            l = self.light[(y*self.size)+x] + self.get_daylight()
            if l > 0xFF:
                l = 0xFF
        glColor3ub(l, l, l)

    def set_color_corner(self, (x, y)):
        ltl = 0
        ltr = 0
        lbl = 0
        lbr = 0
        if x > 0 and y > 0:
            ltl = self.light[((y-1)*self.size)+x-1]
        if x < self.size and y > 0:
            ltr = self.light[((y-1)*self.size)+x]
        if x > 0 and y < self.size:
            lbl = self.light[(y*self.size)+x-1]
        if x < self.size and y < self.size:
            lbl = self.light[(y*self.size)+x]
        l = (ltl + ltr + lbl + ltr) / 4 + self.get_daylight()
        if l > 0xFF:
            glColor3ub(0xFF, 0xFF, 0xFF)
        else:
            glColor3ub(l, l, l)
            
    def render_case(self, x, y):
        tile = self.tile[(y*self.size)+x]
        if tile == 2:
            glBindTexture(GL_TEXTURE_2D, self.sprite_water[self.id_sprite_water])
        elif tile == 4:
            glBindTexture(GL_TEXTURE_2D, self.sprite_water[self.id_sprite_water+4])
        elif tile == 6:
            glBindTexture(GL_TEXTURE_2D, self.sprite_water[self.id_sprite_water+8])
        else:
            glBindTexture(GL_TEXTURE_2D, self.sprite_floor[tile])
        z = self.temp_tiles[self.tile[(y*self.size)+x]][3]
        glPushMatrix()
        glTranslated(x, y, z)
        glBegin(GL_QUADS)
        self.set_color_corner((x, y+1))
        glTexCoord2d(0,0); glVertex3d(0, 1, 0)
        self.set_color_corner((x, y))
        glTexCoord2d(0,1); glVertex3d(0, 0, 0)
        self.set_color_corner((x+1, y))
        glTexCoord2d(1,1); glVertex3d(1, 0, 0)
        self.set_color_corner((x+1, y+1))
        glTexCoord2d(1,0); glVertex3d(1, 1, 0)
        glEnd()
        self.set_color((x, y))
        #glCallList(self.list_floor)
        if (not self.tile[(y*self.size)+x] in [2, 4]):
            if (y >= self.size-1 or self.temp_tiles[self.tile[((y+1)*self.size)+x]][3] < z):
                glPushMatrix()
                glTranslated(0, 1, -1)
                glCallList(self.list_floor_side)
                glPopMatrix()
            if (y <= 0 or self.temp_tiles[self.tile[((y-1)*self.size)+x]][3] < z):
                glPushMatrix()
                glTranslated(0, 0, -1)
                glCallList(self.list_floor_side)
                glPopMatrix()
            if (x <= 0 or self.temp_tiles[self.tile[(y*self.size)+x-1]][3] < z):
                glPushMatrix()
                glTranslated(0, 0, -1)
                glRotated(90, 0, 0, 1)
                glCallList(self.list_floor_side)
                glPopMatrix()
            if (x >= self.size-1 or self.temp_tiles[self.tile[(y*self.size)+x+1]][3] < z):
                glPushMatrix()
                glTranslated(1, 0, -1)
                glRotated(90, 0, 0, 1)
                glCallList(self.list_floor_side)
                glPopMatrix()
        glPopMatrix()

    def render_elt(self, x, y, p_x, p_y): 
        id_elt = self.tile_elts[(y*self.size)+x]
        glBindTexture(GL_TEXTURE_2D, self.sprite_elts[id_elt])
        z = self.temp_tiles[self.tile[(y*self.size)+x]][3]
        model = self.temp_elts[self.tile_elts[(y*self.size)+x]][3]
        glPushMatrix()
        glTranslated(x, y, z)
        if model == 1:
            glCallList(self.list_elt_cube)
        elif model == 2:
            glScalef(1, 1, 0.2)
            glCallList(self.list_elt_cube)
        else:
            glTranslated(0.5, 0.5, 0)
            if (p_x-x != 0):
                if (p_x >= x):
                    glRotated(math.atan(float(p_y-y)/float(p_x-x))*180.0/3.14, 0, 0, 1)
                else:
                    glRotated(math.atan(float(p_y-y)/float(p_x-x))*180.0/3.14 + 180, 0, 0, 1)
            else:
                glRotated(90, 0, 0, 1)
            glCallList(self.list_elt)
        glPopMatrix()

    def render_robot(self, r):
        glBindTexture(GL_TEXTURE_2D, self.sprite_robots[r.id_sprite])
        z = self.temp_tiles[self.tile[(r.pos_y*self.size)+r.pos_x]][3] + 0.5 + r.pos_z
        glPushMatrix()
        glTranslated(r.real_pos_x+0.5, r.real_pos_y+0.5, z)
        glRotated(r.angle, 0, 0, 1)
        glCallList(self.list_robot)
        glPopMatrix()

    def render(self, (select_x, select_y, cam_w, cam_h), (dx, dy)):
        p_x = -1.5
        p_y = 5
        p_z = 9
        glMatrixMode(GL_PROJECTION)
        glPushMatrix()
        glLoadIdentity()
        gluPerspective(40, cam_w/cam_h,1,64)
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_ALPHA_TEST);
        glAlphaFunc(GL_GREATER, 127.0/255.0);

        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        gluLookAt(p_x, p_y, p_z,\
                      0, 0, 0, \
                      0, 0, 1)
        glRotated(self.angle, 0, 0, 1)

        z = self.temp_tiles[self.tile[(select_y*self.size)+select_x]][3] + 0.1
        glBindTexture(GL_TEXTURE_2D, self.cursor)
        glPushMatrix()
        glTranslated(-dx, -dy, z)
        glCallList(self.list_select)
        glPopMatrix()
        glTranslated(-select_x-0.5-dx, -select_y-0.5-dy, 0);

        for y in range(max(select_y-6, 0), min(select_y+5, self.size)):
            for x in range(max(select_x-4, 0), min(select_x+7, self.size)):
                self.render_case(x, y)
                if self.tile_elts[(y*self.size)+x] != 0xFF:
                    self.render_elt(x, y, p_x+select_x, p_y+select_y)
        glColor3ub(0xFF, 0xFF, 0xFF)
        for i in self.robots:
            x = i.pos_x
            y = i.pos_y
            if x >= select_x-8 and \
                    y >= select_y-16 and \
                    x < select_x+16 and \
                    y < select_y+8:
                self.render_robot(i)
        glMatrixMode(GL_PROJECTION)
        glPopMatrix()

        glDisable(GL_DEPTH_TEST)
        glDisable(GL_ALPHA_TEST);

    def render_mini(self, surface, (dx, dy)):
        for y in range(self.size):
            for x in range(self.size):
                surface.blit(self.sprite_floor_mini[self.tile[(y*self.size)+x]], (x*2+dx, y*2+dy))

    def build(self, biome):
        n = noise.Noise(33, 33, 8, 8)
        for y in range(self.size):
            for x in range(self.size):
                self.tile[(y*self.size)+x] = self.get_rndtile(x, y, n, biome)
        self.build_elts()
        self.land_mothership()
                
    def get_rndtile(self, x, y, noise, biome):
        val = noise.smooth_noise(x, y, 0.6)
        for i in self.biomes[biome][0]:
            if val < i[0]:
                return i[1]
        return 0

    def build_elts(self):
        biome = self.biome
        n = noise.Noise(33, 33, 8, 8)
        for y in range(self.size):
            for x in range(self.size):
                elt = self.get_rndelts(x, y, n, biome)
                tile = self.tile[(y*self.size)+x]
                self.tile_elts[(y*self.size)+x] = 0xFF
                if elt in [0, 1]:
                    if tile in [0,1]:
                        self.tile_elts[(y*self.size)+x] = elt
                elif elt in [2, 3, 4, 5]:
                    if tile in [0,1, 5, 7, 8, 9, 10, 11]:
                        self.tile_elts[(y*self.size)+x] = elt
                else:
                    self.tile_elts[(y*self.size)+x] = elt
                
    def get_rndelts(self, x, y, noise, biome):
        val = noise.smooth_noise(x, y, 0.7)
        for i in self.biomes[biome][1]:
            if val < i[0]:
                return i[1]
        return 0xFF

    def land_mothership(self):
        r = mothership.Mothership(True,\
                                      self.path,\
                                      "Mothership_"+str(self.x)+"-"+str(self.y),\
                                      self, \
                                      (0, 0),\
                                      self.events)
        
        x = random.randint(0, self.size-1)
        y = random.randint(0, self.size-1)
        while r.move_to((x, y)) == 0:
            x = random.randint(0, self.size-1)
            y = random.randint(0, self.size-1)
        r.real_pos_x = float(x)
        r.real_pos_y = float(y)
        self.robots.append(r)
        self.calculate_light()

    def get_pos_robot(self, n):
        if n < len(self.robots):
            return self.robots[n].get_pos()
        else:
            return (0,0)

    def get_pos_robots(self):
        return [ i.get_pos() for i in self.robots]
            
    def update_robots(self, n, display, events):
        for i in range(len(self.robots)):
            self.robots[i].update((i == n), \
                                   display, events)
        #self.robots[self.running_robot].update((self.running_robot == n), \
        #                                           display, events)
        #if n != None and self.running_robot != n:
        #    self.robots[n].update(True, \
        #                              display, events)
        #self.running_robot += 1
        #if self.running_robot >= len(self.robots):
        #    self.running_robot = 0

    def update_sprite(self):
        if self.tps_anim_sprite <= 0:
            self.tps_anim_sprite = 25
            self.id_sprite_water += 1
            if self.id_sprite_water > 3:
                self.id_sprite_water = 0
        else:
            self.tps_anim_sprite -= 1
