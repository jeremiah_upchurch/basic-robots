##
## game.py for BASIC-RoBots in /home/surply_p
## 
## Copyright (C) 2012 Pierre Surply
## <pierre.surply@gmail.com>
##
## This file is part of BASIC-RoBots.
##
##    BASIC-RoBots is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    BASIC-RoBots is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with BASIC-RoBots.  If not, see <http://www.gnu.org/licenses/>.
##  
## Started on  Thu Jun 28 15:31:47 2012 Pierre Surply
## Last update Tue Sep  4 11:10:51 2012 Pierre Surply
##

import pygame
from pygame.locals import *

import display
import events
import env
import worldmap

class Game:
    def __init__(self, events, env):
        self.env = env
        self.events = events
        self.select_x, self.select_y = env.get_pos_robot(0)
        self.select_robot = None
        self.events.rec_char = False
        self.goto_dashboard = False
        
    def update(self, display, events):
        if events.get_key_once(K_ESCAPE):
            if self.select_robot != None:
                self.select_robot = None
                self.events.rec_char = False
            else:
                self.goto_dashboard = True
        if events.key[K_LCTRL]:
            if events.get_key_once(K_p):
                self.incr_select_robot(-1)
            elif events.get_key_once(K_n):
                self.incr_select_robot(1)
        self.env.update_robots(self.select_robot, display, events)
        self.env.update_sprite()
        if self.select_robot != None:
            self.select_x, self.select_y = self.env.get_pos_robot(self.select_robot)
            self.env.render((self.select_x, self.select_y, \
                                     display.window.get_width(), \
                                     display.window.get_height()), \
                                (2+self.env.robots[self.select_robot].real_pos_x-self.select_x, \
                                     self.env.robots[self.select_robot].real_pos_y-self.select_y))
            display.render_terminal()
        else:
            self.handle_events(events)
            self.env.render((self.select_x, self.select_y, \
                                     display.window.get_width(), \
                                     display.window.get_height()), \
                                (0,0))
        display.flip()
        return self.goto_dashboard
        

    def handle_events(self, events):
        if events.get_key_once(K_RETURN):
            self.env.angle = 0
            for i in range(len(self.env.robots)):
                if self.env.robots[i].get_pos() == (self.select_x, \
                                                      self.select_y):
                    self.select_robot = i
                    self.events.rec_char = True
        if events.key[K_LCTRL]:
            if events.key[K_r]:
                self.env.angle += 1
            arrows = (events.key[K_UP], \
                          events.key[K_DOWN],\
                          events.key[K_LEFT],\
                          events.key[K_RIGHT])
        else:
            arrows = (events.get_key_once(K_UP), \
                          events.get_key_once(K_DOWN),\
                          events.get_key_once(K_LEFT),\
                          events.get_key_once(K_RIGHT))
        if arrows[0] and self.select_y > 0:
            self.select_y -= 1
        if arrows[1] and self.select_y < self.env.size-1:
            self.select_y += 1
        if arrows[3] and self.select_x > 0:
            self.select_x -= 1
        if arrows[2] and self.select_x < self.env.size-1:
            self.select_x += 1
        

    def incr_select_robot(self, incr):
        self.events.rec_char = True
        if self.select_robot == None:
            self.select_robot = 0
        else:
            self.select_robot += incr
            if self.select_robot > len(self.env.robots)-1:
                self.select_robot = 0
            elif self.select_robot < 0:
                self.select_robot = len(self.env.robots)-1
