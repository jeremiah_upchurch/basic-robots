##
## menu.py for BASIC-RoBots
##
## Copyright (C) 2012 Pierre Surply
## <pierre.surply@gmail.com>
##
## This file is part of BASIC-RoBots.
##
##    BASIC-RoBots is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    BASIC-RoBots is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with BASIC-RoBots.  If not, see <http://www.gnu.org/licenses/>.
##
## Started on  Sat Jun 30 12:05:55 2012 Pierre Surply
## Last update Wed Sep  5 14:13:17 2012 Pierre Surply
##

import os
import pygame
from pygame.locals import *

import terminal

class Menu:
    entry = [("New game", K_n, 0),
                 ("Load game", K_l, 1),
                 ("Quit", K_q, 2)]

    def __init__(self, events, display):
        self.terminal = terminal.Terminal(events)
        self.terminal.set_title("BASIC-RoBots")
        self.show_menu()
        self.stack = []

    def update(self, display, events):
        w = display.window.get_width()/8-1
        h = display.window.get_height()/12-1
        self.handle_events(events)
        self.terminal.display(display, (0, 0, w, h))
        display.render_terminal(True)
        display.flip()
        if len(self.stack) > 0:
            if self.stack[-1] in [0, 1]:
                cmd = self.terminal.prompt()
                if cmd != None:
                    return self.handle_prompt(self.stack[-1], cmd)
                else:
                    return None
            elif self.stack[-1] == 2:
                return (2, "")
        else:
            return None

    def handle_prompt(self, state, cmd):
        if state == 0:
            if not os.path.isdir("saves/" + cmd):
                return (state, cmd)
            else:
                self.terminal.write_line(cmd + " already exists")
                return None
        elif state == 1:
            if os.path.isdir("saves/" + cmd):
                return (state, cmd)
            else:
                self.terminal.write_line("Can't load " + cmd)
                self.terminal.start_prompt("?")
                return None

    def handle_events(self, events):
        if len(self.stack) == 0:
            for i in self.entry:
                if events.get_key_once(i[1]):
                    self.choose(i[2])
        else:
            if events.get_key_once(K_ESCAPE):
                self.stack.pop()
                self.terminal.end_prompt(events)
                self.terminal.clear()
                self.show_menu()

    def title(self):
        title = """  ____           _____ _____ _____      _____       ____        _       
 |  _ \   /\    / ____|_   _/ ____|    |  __ \     |  _ \      | |      
 | |_) | /  \  | (___   | || |   ______| |__) |___ | |_) | ___ | |_ ___ 
 |  _ < / /\ \  \___ \  | || |  |______|  _  // _ \|  _ < / _ \| __/ __|
 | |_) / ____ \ ____) |_| || |____     | | \ \ (_) | |_) | (_) | |_\__ \\
 |____/_/    \_\_____/|_____\_____|    |_|  \_\___/|____/ \___/ \__|___/\n """
        author = "   Pierre SURPLY (pierre.surply@gmail.com) - 2012"
        self.terminal.write_line(title)
        self.terminal.write_line(author)

    def show_menu(self):
        self.title()
        self.terminal.write("\n\n")
        for i in self.entry:
            self.terminal.write("   \x10  ")
            self.terminal.write(pygame.key.name(i[1]), 1)
            self.terminal.write_line(": " + i[0])
        self.terminal.write("\n\n")
            

    def choose(self, n):
        if len(self.stack) == 0:
            self.stack.append(n)
            if n == 0:
                self.terminal.write("New game\n", 1)
                self.terminal.write("World name ?\n\n")
                self.terminal.start_prompt("?")
            elif n == 1:
                self.start_load()

            
    def start_load(self):
        self.terminal.write("Load game\n\n", 1)
        if not os.path.isdir("saves/"):
            os.mkdir("saves")
        for i in os.listdir("saves/"):
            self.terminal.write("   \x10  ")
            self.terminal.write_line(i)
        self.terminal.write("\n\n")
        self.terminal.start_prompt("?")
