##
## thermalpowerstation.py for BASIC-RoBots
## 
## Copyright (C) 2012 Pierre Surply
## <pierre.surply@gmail.com>
##
## This file is part of BASIC-RoBots.
##
##    BASIC-RoBots is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    BASIC-RoBots is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with BASIC-RoBots.  If not, see <http://www.gnu.org/licenses/>.
## 
## Started on  Mon Jul 23 11:55:10 2012 Pierre Surply
## Last update Wed Sep  5 17:01:49 2012 Pierre Surply
##

import robot

class ThermalPowerStation(robot.Robot):
    list_comb = [(0, 50)]
    def __init__(self, new, world, name, env, (x, y), events):
        robot.Robot.__init__(self, new, world, name, env, (x, y), events)
        self.inv.capacity = 500000
        self.id_sprite = 3
        self.ext_cmd["burn"] = (self.burn,\
                                    "Burns fuel to produce energy",\
                                    "",\
                                    (0, []),\
                                    {"A" : "item id"})
        self.ext_cmd["fuels"] = (self.fuels,\
                                     "Lists all types of fuels",\
                                     "Always returns 0",\
                                     (0, []),\
                                     {})
        self.ext_cmd["disten"] = (self.disten,\
                                     "Distributes energy with near robots",\
                                     "Always returns 0",\
                                     (0, []),\
                                     {})
    def incr_energy(self, incr):
        self.energy += incr
        if self.energy <= 0:
            self.energy = 1
        elif self.energy > 255:
            self.energy = 255

    def burn(self):
        id_comb = self.mem["A"]
        for i in self.list_comb:
            if i[0] == id_comb:
                if self.del_item(id_comb, 1):
                    self.incr_energy(i[1])
                    return i[1]
                else:
                    return 0
        return 0

    def fuels(self):
        self.os.terminal.write("Id", 1)
        self.os.terminal.write("  ")
        self.os.terminal.write("Item", 1)
        self.os.terminal.write("        ")
        self.os.terminal.write("Energy\n", 1)
        s = ""
        for i in self.list_comb:
            item = self.inv.id2item(i[0])
            s_id = str(i[0])
            s += s_id + (" " * (4-len(s_id))) + item[0] + (" " * (12-len(item[0]))) + str(i[1]) + "\n"
        self.os.terminal.write(s)
        return 0

    def disten(self):
        nbr_robots = 0
        for i in self.env.robots:
            if ((i.pos_x - self.pos_x) ** 2 + \
                    (i.pos_y - self.pos_y) ** 2 < 100) and \
                    i.energy < 0xFF:
                i.incr_energy(1)
                self.incr_energy(-1)
                nbr_robots += 1
        return nbr_robots
