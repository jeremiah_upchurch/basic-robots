##
## basic.py for BASIC-RoBots
##
## Copyright (C) 2012 Pierre Surply
## <pierre.surply@gmail.com>
##
## This file is part of BASIC-RoBots.
##
##    BASIC-RoBots is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    BASIC-RoBots is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with BASIC-RoBots.  If not, see <http://www.gnu.org/licenses/>.
## 
## Started on  Sat May  5 13:04:41 2012 Pierre Surply
## Last update Fri Dec 21 14:57:09 2012 Pierre Surply
##

import random
import time

KeyInterrupt =  0
KeyPressed =    1

class Basic:
    func = {"rand" : (lambda: random.randint(0, 255)),\
                "time" : (lambda: int(time.time()) % 256)}
    def __init__(self, current_dir, src, robot, terminal, debug=False):
        self.current_dir = current_dir
        self.src = src
        self.terminal = terminal
        self.robot = robot
        self.mem = robot.mem
        self.mem_stack = robot.mem_stack
        self.ext_cmd = self.robot.ext_cmd
        self.callstack = []
        self.whilestack = []
        self.opr = self.set_opr(self.src)
        self.line = 0
        self.debug = debug

    def set_opr(self, src):
        opr = []
        for i in src.split("\n"):
            opr.append(filter(lambda x: x != "", i.split(" ")))
        return opr
    
    def print_debug(self):
        if self.line < len(self.opr):
            self.terminal.clear()
            self.terminal.write_line("\x04\x04\x04 Basic Debug \x04\x04\x04", 1)
            self.terminal.write_line("Press N to execute next instruction")
            self.terminal.write_line("Press R to quit debugger\n")
            self.terminal.write_line("Instructions (line " + str(self.line) + ")", 1)
            for i in self.opr[self.line-2:self.line]:
                self.terminal.write_line("   " + " ".join(i))
            self.terminal.write_line("\x1A  " + " ".join(self.opr[self.line]))
            for i in self.opr[self.line+1:self.line+5]:
                self.terminal.write_line("   " + " ".join(i))
            self.terminal.write_line("\n", 1)
            self.terminal.write_line("Registers", 1)
            for i in sorted(self.robot.mem.keys()):
                self.terminal.write(" ")
                self.terminal.write(i, 1)
                self.terminal.write(":" + hex(self.robot.mem[i]))
            self.terminal.write_line("\n")
            self.terminal.write_line("Registers (RO)", 1)
            mem_ro = self.robot.get_mem_ro()
            for i in sorted(mem_ro):
                self.terminal.write(" ")
                self.terminal.write(i, 1)
                self.terminal.write(":" + hex(mem_ro[i]))
            self.terminal.write_line("\n")
            self.terminal.write_line("Stack", 1)
            if self.robot.mem_stack == []:
                self.terminal.write_line("The stack is empty")
            else:
                for i in self.robot.mem_stack[-5::]:
                    self.terminal.write("\xB3" + hex(i))
                self.terminal.write_line(" \x1B")
            self.terminal.write_line("\nOut", 1)
            
    def eval_line(self):
        if self.line < len(self.opr):
            opr = self.opr[self.line]
            if len(opr) > 0:
                cmd = opr[0].upper()
                if cmd == "PRINT":
                    self.write(opr, self.line)
                elif cmd == "READ":
                    try:
                        self.line = self.read(opr[1], self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif cmd == "INCLUDE":
                    try:
                        self.line = self.include(self.current_dir + opr[1], self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif cmd == "LET":
                    try:
                        var = opr[1]
                        s = ""
                        for i in opr[2::]:
                            s += i + " "
                        self.line = self.let(var, s, self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 2, self.line)
                elif cmd == "GOTO":
                    try:
                        label = opr[1]
                        self.line = self.goto_label(label, self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif cmd == "GOTOSUB":
                    try:
                        label = opr[1]
                        self.callstack.append(self.line)
                        self.line = self.goto_label(label, self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif cmd == "RETURN":
                    if len(self.callstack) > 0 :
                        self.line = self.callstack.pop()
                elif cmd == "WHILE":
                    try:
                        self.line = self.while_loop_begin(opr[1::], self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif cmd == "ENDWHILE":
                    self.line = self.while_loop_end(self.line)
                elif cmd == "IF":
                    self.line = self.alt(opr[1::], self.line)
                elif cmd == "CALL":
                    try:
                        if len(opr) < 3:
                            self.line = self.call(opr[1], None, self.line)
                        else:
                            self.line = self.call(opr[1], opr[2], self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif cmd == "PUSH":
                    try:
                        self.line = self.push(opr[1], self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif cmd == "POP":
                    try:
                        self.line = self.pop(opr[1], self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif not(cmd in ["LABEL", "REM"]):
                    self.line = self.raise_error("Unknown command \"" + cmd + "\"" , self.line)
            self.line += 1
            return True
        else:
            return False

    def write(self, opr, line):
        s = ""
        for i in opr[1::]:
            if len(i) > 1 and i[0] == '$':
                s += str(self.eval_exp(i[1::], line)) + " "
            else:
                s += i + " "
        self.terminal.write_line(s)

    def read(self, var, line):
        if var in self.mem:
            if not self.terminal.act_prompt:
                self.terminal.start_prompt(var + "?")
            cmd = self.terminal.prompt()
            if cmd != None:
                try:
                    self.mem[var] = int(eval(cmd))
                except:
                    self.mem[var] = 0
                return line
            else:
                return line-1
        else:
            return self.raise_error_reg(var, line)

    def let(self, var, expr, line):
        return self.set_mem(var, self.eval_exp(expr, line), line)

    def set_mem(self, var, val, line):
        if var in self.mem:
            self.mem[var] = val
            self.mem["O"] = 0
            if self.mem[var] > 255:
                self.mem[var] %= 0x100
                self.mem["O"] = 1
            elif self.mem[var] < 0:
                self.mem[var] = 0
                self.mem["O"] = 1
            return line
        else:
            return self.raise_error_reg(var, line)

    def goto_return(self, label, line):
        return self.goto(label, "GOTOSUB", line)

    def goto_label(self, label, line):
        return self.goto(label, "LABEL", line)

    def goto_interrupt(self, label, line):
        return self.goto(label, "LABEL", line, False)

    def goto(self, label, cmd, line, verb=True):
        i = 0
        while i < len(self.opr) and\
                (len(self.opr[i]) < 1  or \
                     (self.opr[i][0].upper() != cmd) or \
                     (self.opr[i][1] != label)):
            i += 1
        if i < len(self.opr):
            return i
        elif verb:
            return self.raise_error("The " + cmd.lower() + " \"" + label +"\" can not be found", line)
        else:
            return -1

    def interrupt(self, interrupt):
        new_line = self.line
        if interrupt == KeyInterrupt:
            new_line = self.goto_interrupt(":KeyInterrupt", self.line)
        elif interrupt == KeyPressed:
            new_line = self.goto_interrupt(":KeyPressed", self.line)
        if new_line != -1:
            self.line = new_line
        elif interrupt == KeyInterrupt:
            self.terminal.write_line("Killed", 6)
            self.line = len(self.opr)
            
    def alt(self, opr, line):
        boolean = []
        label_then = ""
        label_else = ""
        i=0
        while i < len(opr) and opr[i].upper() != "THEN":
            boolean.append(opr[i])
            i += 1
        if len(opr) <= i+1:
            return self.raise_error("Syntax error", line)
        else:
            i += 1
            label_then = opr[i]
            if len(opr) > i+1:
                i += 1
                if opr[i].upper() == "ELSE" and len(opr) > i+1:
                    label_else = opr[i+1]
                else:
                    return self.raise_error("Syntax error", line)
            if self.eval_boolean("".join(boolean), line):
                return self.goto_label(label_then, line)
            elif label_else != "":
                return self.goto_label(label_else, line)
            else:
                return line

    def while_loop_begin(self, opr, line):
        if self.eval_boolean("".join(opr), line):
            self.whilestack.append(line)
            return line
        else:
            nbr_loop = 0
            i = line+1
            while i < len(self.opr) and\
                    (len(self.opr[i]) < 1  or \
                         (self.opr[i][0].upper() != "ENDWHILE") or \
                         (nbr_loop > 0)):
                if len(self.opr[i]) >= 1:
                    cmd = self.opr[i][0].upper()
                    if (cmd == "WHILE"):
                        nbr_loop += 1
                    elif (cmd == "ENDWHILE"):
                        nbr_loop -= 1
                i += 1
            if i < len(self.opr):
                return i
            else:
                return self.raise_error("", line)

    def while_loop_end(self, line):
        return self.whilestack.pop()-1

    def eval_exp(self, exp, line):
        try:
            return int(eval(exp, {"__builtins__":None}, dict(self.mem, \
                                                                 **dict(self.func, **self.robot.get_mem_ro()))))
        except:
            self.raise_warning("Invalid Expression", line)
            return 0

    def eval_boolean(self, exp, line):
        try:
            return eval(exp, {"__builtins__":None}, dict(self.mem, **dict(self.func, **self.robot.get_mem_ro())))
        except:
            self.raise_warning("Invalid Boolean", line)
            return False

    def include(self, path, line):
        try:
            src = open(path, 'r').read()
            self.opr = self.opr[0:line] + self.set_opr(src) + self.opr[line+1:]
            return line-1
        except IOError:
            return self.raise_error("The program " + path + " can't be found", line)
        
    def call(self, function, reg, line):
        if function in self.ext_cmd:
            if reg == None:
                self.robot.run_ext_cmd(self.ext_cmd[function][0], \
                                           self.ext_cmd[function][3])
                return line
            else:
                return self.set_mem(reg, self.robot.run_ext_cmd(self.ext_cmd[function][0], \
                                                                 self.ext_cmd[function][3]), line)
        else:
            return self.raise_error("The external command \""+ function + "\"is not defined for this robot", line)

    def push(self, reg, line):
        if reg in self.mem:
            self.mem_stack.append(self.mem[reg])
            return line
        else:
            return self.raise_error_reg(reg, line)

    def pop(self, reg, line):
        if reg in self.mem:
            if self.mem_stack != []:
                self.mem[reg] = self.mem_stack.pop()
            return line
        else:
            return self.raise_error_reg(reg, line)
            
            
    def raise_error_reg(self, reg, line):
        return self.raise_error("The robot has not the \"" + reg + "\" register", line)

    def raise_error_arg(self, cmd, nbr_arg, line):
        self.raise_error("The command \"" + cmd + "\" expect " + str(nbr_arg) + " argument(s)", line)
        
    def raise_error(self, msg, line):
        self.terminal.write_line("** Error **", 6)
        self.terminal.write_line("** Line " + str(line + 1) + ": " + msg, 6)
        return len(self.opr)

    def raise_warning(self, msg, line):
        self.terminal.write_line("** Warning **", 5)
        self.terminal.write_line("** Line " + str(line + 1) + ": " + msg, 5)
