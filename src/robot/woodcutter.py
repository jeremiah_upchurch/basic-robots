##
## woodcutter.py for BASIC-RoBots
## 
## Copyright (C) 2012 Pierre Surply
## <pierre.surply@gmail.com>
##
## This file is part of BASIC-RoBots.
##
##    BASIC-RoBots is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    BASIC-RoBots is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with BASIC-RoBots.  If not, see <http://www.gnu.org/licenses/>.
## 
## Started on  Tue Jul  3 14:38:15 2012 Pierre Surply
## Last update Wed Sep  5 17:01:30 2012 Pierre Surply
##

import robot

class Woodcutter(robot.Robot):
    def __init__(self, new, world, name, env, (x, y), events):
        robot.Robot.__init__(self, new, world, name, env, (x, y), events)
        self.inv.capacity = 100000
        self.id_sprite = 1
        self.ext_cmd["cuttree"] = (self.cut_wood,\
                                       "Cuts a tree to make wood",\
                                       "1 if a tree has been cut, 0 otherwise", \
                                       (2, []), \
                                       {})

    def cut_wood(self):
        pos = self.get_pos_forward()
        elt = self.env.tile_elts[(pos[1]*self.env.size)+pos[0]]
        if elt in [0, 1, 2, 3, 4]:
            if elt == 4: 
                self.env.tile_elts[(pos[1]*self.env.size)+pos[0]] = None
                self.add_item(0, 2)
            else:
                self.env.tile_elts[(pos[1]*self.env.size)+pos[0]] = 4
                if elt in [2, 3]:
                    self.add_item(0, 4)
                    self.add_item_rnd(1, 2, 3)
                else:
                    self.add_item(0, 8)
                    self.add_item_rnd(1, 2, 2)
            return 1
        else:
            return 0
