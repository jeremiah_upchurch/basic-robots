##
## inventory.py for BASIC-RoBots
## 
## Copyright (C) 2012 Pierre Surply
## <pierre.surply@gmail.com>
##
## This file is part of BASIC-RoBots.
##
##    BASIC-RoBots is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    BASIC-RoBots is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with BASIC-RoBots.  If not, see <http://www.gnu.org/licenses/>.
##  
## Started on  Sun Jul  8 21:20:03 2012 Pierre Surply
## Last update Sat Aug 11 13:41:25 2012 Pierre Surply
##
import os

class Inventory:
    temp_items = [("Wood", 10000),\
                      ("Sapling", 2),\
                      ("Seed", 1)]
    def __init__(self, path, capacity):
        self.path = path
        self.items = {}
        self.capacity = capacity
        self.weight = 0

    def set_item(self, item, qte):
        it = self.temp_items[item][0]
        if it in self.items:
            qte += self.items[it]
        if qte > 0:
            self.items[it] = qte
            self.calc_weight()
            return True
        elif qte == 0:
            del self.items[it]
            self.calc_weight()
            return True
        else:
            return False

    def calc_weight(self):
        weight = 0
        for i in self.items.keys():
            for ti in self.temp_items:
                if ti[0] == i:
                    weight += ti[1]*self.items[i]
        self.weight = weight

    def isfull(self):
        return self.weight > self.capacity

    def item2id(self, name):
        for i in range(len(self.temp_items)):
            if self.temp_items[i][0] == name:
                return i

    def id2item(self, id_item):
        if id_item < len(self.temp_items):
            return self.temp_items[id_item]
        else:
            return None

    def __str__(self):
        s = ""
        for i in self.items.keys():
            s_id = str(self.item2id(i))
            s += s_id + (" " * (4-len(s_id))) + i + (" " * (12-len(i))) + str(self.items[i]) + "\n"
        return s

    def save(self):
        f = open(self.path, "w") 
        s = ""
        for i in self.items.keys():
            s += i + ":" + str(self.items[i]) + "\n"
        f.write(s)
        f.close()

    def parse(self, s):
        for item in s.split("\n"):
            if len(item) > 1:
                name, qte = tuple(item.split(":"))
                qte = int(qte)
                self.items[name] = qte
        self.calc_weight()
