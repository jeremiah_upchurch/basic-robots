##
## pathfinding.py for BASIC-RoBots in /home/surply_p
##
## Copyright (C) 2012 Pierre Surply
## <pierre.surply@gmail.com>
##
## This file is part of BASIC-RoBots.
##
##    BASIC-RoBots is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    BASIC-RoBots is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with BASIC-RoBots.  If not, see <http://www.gnu.org/licenses/>.
##
## Started on  Fri Aug 17 15:44:46 2012 Pierre Surply
## Last update Sat Aug 18 10:32:29 2012 Pierre Surply
##

import heapq
import math

class Node:
    def __init__(self, (x, y)):
        self.pos = (x, y)
        self.h = 0
        self.k = 0
        self.g = 0
        self.parent = None

def build_graph(f_coll, (w, h)):
    graph = [[None] * h for i in range(w)]
    for y in range(h):
        for x in range(w):
            graph[x][y] = Node((x, y))
            if  f_coll((x, y)):
                graph[x][y].k = None
            else:
                graph[x][y].k = 0
    return graph

def calc_dist((start_x, start_y), (end_x, end_y)):
    return int(math.sqrt((start_x - end_x)**2 + (start_y - end_y)**2))

def mini_h(l):
    mini = 0
    for i in range(len(l)):
        if l[i].h < l[mini].h:
            mini = i
    return mini

def add_succ(s, node):
    if node.k != None:
        s.append(node)

def succ(n, graph, (w, h)):
    s = []
    x = n.pos[0]
    y = n.pos[1]
    if x > 0:
        add_succ(s, graph[x-1][y])
    if x < w-1:
        add_succ(s, graph[x+1][y])
    if y > 0:
        add_succ(s, graph[x][y-1])
    if y < h-1:
        add_succ(s, graph[x][y+1])
    return s

def build_path(node):
    path = []
    while node != None:
        path.append(node.pos)
        node = node.parent
    path.reverse()
    return path

def findpath(f_coll, (w, h), (start_x, start_y), (end_x, end_y), it_max):
    it = 0
    graph = build_graph(f_coll, (w, h))
    openlist = []
    heapq.heappush(openlist, (0, graph[start_x][start_y]))
    closelist = []

    if graph[end_x][end_y].k == None:
        s = succ(graph[end_x][end_y], graph, (w, h))
        if len(s) > 0:
            end_x, end_y = succ(graph[end_x][end_y], graph, (w, h))[0].pos
        else:
            return None

    while openlist != [] and it < it_max:
        it += 1
        current = heapq.heappop(openlist)[1]
        closelist.append(current)
        if current.pos != (end_x, end_y):
            for i in succ(current, graph, (w, h)):
                if i in closelist:
                    continue
                if not((i.h, i) in openlist) or \
                        i.g > current.g + i.k:
                    i.g = current.g + i.k
                    i.h = i.g + calc_dist(i.pos, (end_x, end_y))
                    i.parent = current
                    heapq.heappush(openlist, (i.h, i))

        else:
            return build_path(current)
