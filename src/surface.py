##
## surface.py for BASIC-RoBots in /home/surply_p
##
## Copyright (C) 2012 Pierre Surply
## <pierre.surply@gmail.com>
##
## This file is part of BASIC-RoBots.
##
##    BASIC-RoBots is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    BASIC-RoBots is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with BASIC-RoBots.  If not, see <http://www.gnu.org/licenses/>.
##
## Started on  Thu Jun 28 13:23:22 2012 Pierre Surply
## Last update Sun Sep  2 20:06:12 2012 Pierre Surply
##

import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *


def surface2texture(surface): 
    texture = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texture)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, \
                     surface.get_width(), \
                     surface.get_height(), \
                     0, GL_RGBA, GL_UNSIGNED_BYTE, \
                     pygame.image.tostring(surface, "RGBA", 1))
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    return texture

def cut_surface(surface, (w, h)):
    l_surface = []
    for y in range(surface.get_height() / h):
        for x in range(surface.get_width() / w):
            new = pygame.Surface((w, h))
            new.blit(surface, (0, 0), pygame.Rect(x*w, y*h, w, h))
            new.set_colorkey(pygame.Color(255, 0, 255, 255))
            l_surface.append(surface2texture(new))
    return l_surface
    
def change_color(surface, front, back = pygame.Color(255, 0, 255, 255)):
    new_surface = surface.copy()
    for y in range(new_surface.get_height()):
        for x in range(new_surface.get_width()):
            c = tuple(new_surface.get_at((x, y)))
            if (c == (255, 255, 255, 255)):
                new_surface.set_at((x, y), front)
            elif (c == (255, 0, 255, 255)):
                new_surface.set_at((x, y), back)
    return new_surface
