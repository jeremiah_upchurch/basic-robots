##
## savable.py for BASIC-RoBots
##
## Copyright (C) 2012 Pierre Surply
## <pierre.surply@gmail.com>
##
## This file is part of BASIC-RoBots.
##
##    BASIC-RoBots is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    BASIC-RoBots is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with BASIC-RoBots.  If not, see <http://www.gnu.org/licenses/>.
##
## Started on  Sat Jun 30 15:34:37 2012 Pierre Surply
## Last update Mon Sep  3 18:04:50 2012 Pierre Surply
##

import pygame
from pygame.locals import *

class Savable:
    def save_img(self, path, size, tile, function):
        surface = pygame.Surface((size, size))
        for y in range(size):
            for x in range(size):
                surface.set_at((x, y), function((x, y)))
        pygame.image.save(surface, path)

    def load_img(self, path, size, tile, function):
        surface = pygame.image.load(path)
        for y in range(min(size, surface.get_height())):
            for x in range(min(size, surface.get_width())):
                tile[(y*size)+x] = function(tuple(surface.get_at((x, y))))
